package io.highcreeksoftware.shortcodes

import java.time.LocalDateTime
import java.time.ZoneOffset
import kotlin.random.Random

fun generateShortCode(length: Int, codeType: CodeType = CodeType.All): String {
    var possible: Array<String> = when(codeType) {
        CodeType.LowerCase -> sourceChars.sliceArray(0..26)
        CodeType.UpperCase -> sourceChars.sliceArray(27..52)
        CodeType.AlphaNumeric -> sourceChars.sliceArray(0..62)
        CodeType.All -> sourceChars
    }

    val rand = Random(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC))

    val builder = StringBuilder()
    for(i in 0 until length) {
        builder.append(possible[rand.nextInt(possible.size)])
    }

    return builder.toString()
}