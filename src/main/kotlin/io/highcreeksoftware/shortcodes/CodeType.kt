package io.highcreeksoftware.shortcodes

enum class CodeType {
    LowerCase, UpperCase, AlphaNumeric, All
}